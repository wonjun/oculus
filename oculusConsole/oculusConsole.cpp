// oculusConsole.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <OVR_CAPI_0_8_0.h>

using namespace std;

int main()
{
	// Initializes LibOVR, and the Rift
	ovrResult result = ovr_Initialize(nullptr);
	if (!OVR_SUCCESS(result)) {
		return 0;
	}

	ovrHmd HMD;
	ovrGraphicsLuid luid;
	result = ovr_Create(&HMD, &luid);
	if (!OVR_SUCCESS(result)) {
		return 0;
	}

	ovrHmdDesc hmdDesc = ovr_GetHmdDesc(HMD);

	// Output some attributes of the HMD.
	cout << "Connected to HMD with name " << hmdDesc.ProductName
		<< " and firmware version " << hmdDesc.FirmwareMajor << "."
		<< hmdDesc.FirmwareMinor << std::endl;

	ovr_Destroy(HMD); 

	ovr_Shutdown();

	return 0;
}

